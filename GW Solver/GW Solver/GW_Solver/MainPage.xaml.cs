﻿using System;
using Xamarin.Forms;

namespace GW_Solver
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public int ReceivedFromXAML;

        void Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (btn.Text == "Yes" && Responses.Text.Contains("1"))
            {
                Responses.Text = "Question 2: Is your person black or do they wear glasses?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("1"))
            {
                Responses.Text = "Question 2: Is your person black or do they have facial hair?";
            }
            //Question 1 done
            else if (btn.Text == "Yes" && Responses.Text.Contains("2") && Responses.Text.Contains("glasses"))
            {
                Responses.Text = "Question 3: Is your person black?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("2") && Responses.Text.Contains("facial"))
            {
                Responses.Text = "Question 3: Is there hair on your person's chin?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("2") && Responses.Text.Contains("glasses"))
            {
                Responses.Text = "Question 3: Is your person's teeth showing?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("2") && Responses.Text.Contains("facial"))
            {
                Responses.Text = "Question 3: Does your person wear hats of glasses?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("3") && Responses.Text.Contains("black"))
            {
                Responses.Text = "Question 4: Is your person wearing a hat?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("3") && Responses.Text.Contains("chin"))
            {
                Responses.Text = "Question 4: Is your person black?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("3") && Responses.Text.Contains("teeth"))
            {
                Responses.Text = "Question 4: Does your person have black hair?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("3") && Responses.Text.Contains("hats"))
            {
                Responses.Text = "Question 4: Does your person wear glasses?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("3") && Responses.Text.Contains("black"))
            {
                Responses.Text = "Question 4: Does your person have a big nose?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("3") && Responses.Text.Contains("chin"))
            {
                Responses.Text = "Question 4: Does your person have white hair?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("3") && Responses.Text.Contains("teeth"))
            {
                Responses.Text = "Question 4: Is your person male?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("3") && Responses.Text.Contains("hats"))
            {
                Responses.Text = "Question 4: Does your person have ginger hair?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("hat"))
            {
                Responses.Text = "Question 5: Are you Daniel?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("black"))
            {
                Responses.Text = "Question 5: Are you James?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("hair"))
            {
                Responses.Text = "Question 5: Are you Tyler?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("glasses"))
            {
                Responses.Text = "Question 5: Are you Joseph?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("nose"))
            {
                Responses.Text = "Question 5: Are you Alex?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("white"))
            {
                Responses.Text = "Question 5: Are you Joshua?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("male"))
            {
                Responses.Text = "Question 5: Are you William?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("4") && Responses.Text.Contains("ginger"))
            {
                Responses.Text = "Question 5: Are you Zachary?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("hat"))
            {
                Responses.Text = "Question 5: Are you Connor?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("black"))
            {
                Responses.Text = "Question 5: Are you David?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("black hair"))
            {
                Responses.Text = "Question 5: Are you Brandon?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("glasses"))
            {
                Responses.Text = "Question 5: Are you Ashley?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("nose"))
            {
                Responses.Text = "Question 5: Are you Nick?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("white hair"))
            {
                Responses.Text = "Question 5: Are you Sarah?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("male"))
            {
                Responses.Text = "Question 5: Are you Megan?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("4") && Responses.Text.Contains("ginger"))
            {
                Responses.Text = "Question 5: Are you Kyle?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("5"))
            {
                Responses.Text = "Congratulations!";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("Daniel") || Responses.Text.Contains("James") || Responses.Text.Contains("Tyler") || Responses.Text.Contains("Joseph") || Responses.Text.Contains("Alex") || Responses.Text.Contains("Joshua") || Responses.Text.Contains("William") || Responses.Text.Contains("Zachary"))
            {
                Responses.Text = "Wait, what? Did someone cheat... DID SOMEONE FUCKING CHEAT?!?!";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Connor"))
            {
                Responses.Text = "Question 6: Are you Andy?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("David"))
            {
                Responses.Text = "Question 6: Are you Jon?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Brandon"))
            {
                Responses.Text = "Question 6: Are you Jake?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Ashley"))
            {
                Responses.Text = "Question 6: Are you Chris?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Nick"))
            {
                Responses.Text = "Question 6: Are you Emily?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Sarah"))
            {
                Responses.Text = "Question 6: Are you Justin?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Megan"))
            {
                Responses.Text = "Question 6: Are you Rachel?";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("5") && Responses.Text.Contains("Kyle"))
            {
                Responses.Text = "Question 6: Are you Matt?";
            }
            else if (btn.Text == "Yes" && Responses.Text.Contains("6"))
            {
                Responses.Text = "Congraultaions!";
            }
            else if (btn.Text == "No" && Responses.Text.Contains("6"))
            {
                Responses.Text = "Wait, what? Did someone cheat... DID SOMEONE FUCKING CHEAT?!?!";
            }
        }

        void Clear(object sender, EventArgs e)
        {
            Responses.Text = "Question 1: Is your person looking left?";
        }

        void Instructions(object sender, EventArgs e)
        {
            DisplayAlert("The Strategy:",
                "The correct strategy is to always remove half of the board. What this does is creates the smallest possible group that we are 100% certain contains the person we are looking for",
                "OK");
        }
    }
}